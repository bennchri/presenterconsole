# Using the PresenterConsole tool 

To start the application you run main in the class `rl.com.presenterconsole.com.PresenterConsole`.
You are required to provide a directory path as an inparam when running. PresenterConsole will look for the files `preso.pdf` (containing the slides) and `notes.pdf` containing the notes. The tool is pre-setup to hook up the result of another presentertool called PresenterBuilder located here (https://bitbucket.org/benc_kau/presenterbuilder).

# Requirements

This tool requires Java version 7 a JRE is enough.


```
#!bash
java rl.com.presenterconsole.com.PresenterConsole path/to/presodir
```

# Keyboard commands 


```
#!bash
next slide: n, <space> and ->
prev. slide: b and <-
Move preso to other screen: s
Go to slide: g
quit app: q
zoom in preso: .
zoom out preso:,
zoom in notes: <ctrl>-.
zoom out notes: <ctrl>-,
zoom in next slide: <shift>-.
zoom out next slide: <shift>-,
```

# Screenshots

## Screenshots
![Presenterconsole](http://img.ctrlv.in/img/525963e3b1642.png)
![Presenterconsole](http://img.ctrlv.in/img/525964541ad06.png)







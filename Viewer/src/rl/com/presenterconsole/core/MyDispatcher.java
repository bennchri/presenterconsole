package rl.com.presenterconsole.core;
import java.awt.KeyEventDispatcher;
import java.awt.event.KeyEvent;


public class MyDispatcher implements KeyEventDispatcher {
	private PresenterConsole v;
	public MyDispatcher(PresenterConsole v) {
		this.v=v;
	}
	@Override
	public boolean dispatchKeyEvent(KeyEvent e) {
		
        if (e.getID() == KeyEvent.KEY_PRESSED) {
        	
            //System.out.println(e.getKeyCode() );
            if (e.getKeyCode()==32||e.getKeyCode()==39||e.getKeyCode()==78) {
            	v.nextSlide();
            	
            }
            if (e.getKeyCode()==37||e.getKeyCode()==66) {
            	v.prevSlide();
            	
            }
            if (e.getKeyCode()==81||e.getKeyCode()==27) {
            	System.exit(1);
            	
            }
            if (e.getKeyCode()==46) {
            	if (e.isShiftDown()) {
            		v.zoomInNext();
            	} else if (e.isControlDown()) {
            		v.zoomInNotes();
            	} else {
            	v.zoomInPreso();
            	}
            }
            if (e.getKeyCode()==44) {
            	if (e.isShiftDown()) {
            		v.zoomOutNext();
            	} else if (e.isControlDown()) {
            		v.zoomOutNotes();
            	} else {
            	v.zoomOutPreso();
            	}
            	
            }
            if (e.getKeyCode()==83) {
            	v.setupPresoDisplay();
            	
            }
            if (e.getKeyCode()==71) {
            	v.gotoSlide();
            	
            }
        }
        return false;
    }
	

}

package rl.com.presenterconsole.core;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.KeyboardFocusManager;
import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.views.DocumentViewController;
import org.icepdf.ri.common.views.DocumentViewControllerImpl;

public class PresenterConsoleView extends JFrame implements PresenterConsole {

	private DocumentViewController nextViewController;
	private DocumentViewController notesViewController;
	static PresenterConsoleView frame;
	private SwingController nextController;
	private SwingController notesController;
	private int currentSlide = 0;
	private JLabel slideNo;
	private ScreenSaverDisabler sd;
	private JFrame window;
	private SwingController presoController;
	private DocumentViewController presoViewController;
	private boolean presoDisplay = false;
	private MyDispatcher keyboardManager;

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		
		try {
			boolean preso=new File(args[0]+"preso.pdf").exists();
			boolean notes=new File(args[0]+"notes.pdf").exists();

			if (!preso||!notes) {
				System.out.println("Make sure you point to a directory containing a 'preso.pdf' and a 'notes.pdf'.");
				System.out.println("A directory ends with a '/' or if you are out of luck a '\'.");
				System.out.println("Redo, do right! ...Now exiting... ");
				System.exit(1);
			}
									
		SwingUtilities.invokeLater(new Runnable() {
		      public void run() {
		    	  frame = new PresenterConsoleView(args);
		  		frame.pack();
		  		frame.setSize(950, 800);
		  		frame.setVisible(true);
		      }
		    });
		}
		catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Make sure you point to a directory containing a 'preso.pdf' and a 'notes.pdf'.");
			System.out.println("A directory ends with a '/' or if you are out of luck a '\'.");
			System.out.println("Redo, do right! ...Now exiting... ");
			System.exit(1);
			
		}
		
		
		
		

	}

	/**
	 * Create the frame.
	 */
	public PresenterConsoleView(String[] args) {
		KeyboardFocusManager manager = KeyboardFocusManager
				.getCurrentKeyboardFocusManager();
		keyboardManager = new MyDispatcher(this);
		manager.addKeyEventDispatcher(keyboardManager);

		getContentPane().setSize(new Dimension(950, 800));
		getContentPane().setLayout(null);
		String dirPath = args[0];
		nextController = new SwingController();
		notesController = new SwingController();
		presoController = new SwingController();
		nextController.setIsEmbeddedComponent(true);
		notesController.setIsEmbeddedComponent(true);
		presoController.setIsEmbeddedComponent(true);
		nextViewController = nextController.getDocumentViewController();
		notesViewController = notesController.getDocumentViewController();
		presoViewController = presoController.getDocumentViewController();
		
		JPanel nextSlideViewPanel = new JPanel();
		nextSlideViewPanel.setBounds(10, 10, 950, 360);
		nextSlideViewPanel.add(nextViewController.getViewContainer());
		getContentPane().add(nextSlideViewPanel);
		
		nextController.openDocument(dirPath + "preso.pdf");
		nextController.showPage(0);
		notesController.openDocument(dirPath + "notes.pdf");
		presoController.openDocument(dirPath + "preso.pdf");
		
		nextController.setPageViewMode(
				DocumentViewControllerImpl.TWO_PAGE_LEFT_VIEW, false);
		notesController.setPageViewMode(
				DocumentViewControllerImpl.ONE_PAGE_VIEW, false);
		presoController.setPageViewMode(
				DocumentViewControllerImpl.ONE_PAGE_VIEW, false);
		
		JPanel notesPanel = new JPanel();
		notesPanel.add(notesViewController.getViewContainer());
		notesPanel.setBounds(10, 365, 780, 480);
		getContentPane().add(notesPanel);

		SimpleDigitalClock clockPanel = new SimpleDigitalClock();
		clockPanel.setBounds(800, 650, 200, 50);
		getContentPane().add(clockPanel);
		slideNo = new JLabel("1");
		slideNo.setFont(new Font("SansSerif", Font.PLAIN, 24));
		slideNo.setBounds(810, 691, 150, 40);
		getContentPane().add(slideNo);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setupPresoDisplay();

		sd = new ScreenSaverDisabler();
		sd.start();
		
	}
	@Override
	public void setupPresoDisplay() {
		presoDisplay=!presoDisplay;
		System.out.println("Display: "+presoDisplay);
		window = new JFrame("Using the Viewer Component");
		KeyboardFocusManager manager = KeyboardFocusManager
				.getCurrentKeyboardFocusManager();
		JPanel presoPanel = new JPanel();
		presoPanel.add(presoViewController.getViewContainer());
		window.getContentPane().add(presoPanel);
		window.pack();
		showOnScreen(presoDisplay,window);
		showOnScreen(!presoDisplay,frame);
		window.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	@Override
	public void redraw() {
		frame.setVisible(true);

	}

	@Override
	public void nextSlide() {
		currentSlide++;

		nextController.showPage(currentSlide);
		notesController.showPage(currentSlide);
		presoController.showPage(currentSlide);

		slideNo.setText(String.valueOf(currentSlide + 1));
	}

	@Override
	public void prevSlide() {
		currentSlide--;
		if (currentSlide < 0) {
			currentSlide = 0;
		}
		nextController.showPage(currentSlide);
		notesController.showPage(currentSlide);
		presoController.showPage(currentSlide);
		slideNo.setText(String.valueOf(currentSlide + 1));

	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		sd.stop();
	}

	public static void showOnScreen(boolean screen, JFrame frame) {
		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		int s;
		if (screen) {
			s=1;
		}
		else {
			s=0;
		}
		GraphicsDevice[] gs = ge.getScreenDevices();
		if (s > -1 && s < gs.length) {
			gs[s].setFullScreenWindow(frame);
		} else if (gs.length > 0) {
			gs[0].setFullScreenWindow(frame);
		} else {
			throw new RuntimeException("No Screens Found");
		}
	}
	

	@Override
	public void zoomInPreso() {
		presoViewController.setZoomIn();
		window.setVisible(true);
	}

	@Override
	public void zoomOutPreso() {
		presoViewController.setZoomOut();
		window.setVisible(true);

	}

	@Override
	public void zoomInNotes() {
		notesViewController.setZoomIn();
		redraw();
	}

	@Override
	public void zoomOutNotes() {
		notesViewController.setZoomOut();
		redraw();

	}

	@Override
	public void zoomInNext() {
		nextViewController.setZoomIn();
		redraw();
	}

	@Override
	public void zoomOutNext() {
		nextViewController.setZoomOut();
		redraw();

	}
	@Override
	public void gotoSlide() {
		presoController.showPageSelectionDialog();
		currentSlide=presoController.getCurrentPageNumber()-1;
		nextSlide();
	}

}

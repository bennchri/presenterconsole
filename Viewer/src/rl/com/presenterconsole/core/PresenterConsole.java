package rl.com.presenterconsole.core;
public interface PresenterConsole {

	public abstract void redraw();

	public abstract void nextSlide();

	public abstract void prevSlide();

	public abstract void zoomInPreso();

	public abstract void zoomOutPreso();

	void zoomOutNotes();

	void zoomOutNext();

	void zoomInNotes();

	void zoomInNext();

	void setupPresoDisplay();

	void gotoSlide();

}